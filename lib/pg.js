'use strict';


const Pg = require('pg');
const Glob = require('glob');

exports.register = function(server, config, next) {

    const client = new Pg.Client(config);

    client.connect((err) => {

        if ( err ) {
            throw err;
        }

        const models = Glob.sync('./app/models/pg/*').map((model) => {
            // hax
            return require( '../' + model)(client);
        });

        Promise.all(models)
        .then(() => {

            server.decorate('request', 'db', client);
            console.log('[postgres] database connected');
            return next();

        }).catch((err) => {

            console.error(err);
            process.exit();
        });

    });

};

exports.register.attributes = {
    name: 'postgres connector',
    version: require('../package.json').version
};
