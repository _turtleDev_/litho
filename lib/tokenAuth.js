'use strict';

const Joi = require('joi');
const Boom = require('boom');

const internals = {};

internals.scheme = function(server, options) {

    const constraints = Joi.object({
        key: Joi.string().required(),
        validate: Joi.func()
    });

    const { error, value } = constraints.validate(options);

    if ( error ) {
        throw new Error(value);
    }

    options = value;

    const authenticate = function(request, reply) {

        if ( !(options.key in request.query) && !request.query[options.key] ) {
            return reply(Boom.forbidden());
        }

        const value = request.query[options.key];
        if ( !options.validate(request, value) ) {
            return reply(Boom.unauthorized());
        }

        return reply.continue({ credentials: value });
    };

    return { authenticate };
};

exports.register = function(plugin, options, next) {

    plugin.auth.scheme('token_auth', internals.scheme);
    plugin.auth.strategy('token', 'token_auth', {
        key: 'access_token',
        validate: function(request, value) {

            return request.authority.token.verify(value);
        }
    });

    return next();
};

exports.register.attributes = {
    name: 'token_auth',
    version: require('../package.json').version
};
