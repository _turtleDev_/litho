'use strict';

import React from 'react';
import Request from './request.js';
import Qs from 'qs';


class App extends React.Component {

    constructor(props) {

        super(props);
        this.state = {
            lat: '',
            long: '',
            message: '',
            messageType: 'message--error',
            selfResult: [],
            postgresResult: []
        };
    }

    handleSubmit(event) {

        event.preventDefault();

        if ( !(this.state.lat && this.state.long) ) {
            this.setState({
                message: 'fill in all the fields'
            });
            return;
        }

        let query = {
            lat: this.state.lat,
            long: this.state.long
        };

        query = Qs.stringify(query);

        const parse = JSON.parse.bind(JSON);
        const requests = [
            Request('GET', '/get_using_self?' + query).then(parse),
            Request('GET', '/get_using_postgres?' + query).then(parse)
        ];

        Promise.all(requests)
        .then((responses) => {
            
            this.setState({
                selfResult: responses[0],
                postgresResult: responses[1]
            });
        });
    }

    update(prop, value) {

        if ( Number.isNaN(Number(value)) ) {
            return;
        }
        this.setState({ [prop]: value, message: '' })

    }

    render() {

        const formStyle = {
            display: 'flex',
            flexDirection: 'column',
            minWidth: 400
        };

        const center = {
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'column'
        };

        const messageStyle = {
            visibility: this.state.message?'visible':'hidden',
            minHeight: 40
        };

        const result = {
            display: 'flex',
            justifyContent: 'space-around',
            width: '100%'
        };

        const renderData = function(data) {
            return (
                <table className="u-full-width u-text-center">
                    <tbody>
                    <tr>
                        <th>Name</th>
                        <th>Lat</th>
                        <th>Long</th>
                    </tr>
                    {data.map((row, idx) => {

                        return (
                            <tr key={idx}>
                                <td>{row.name}</td>
                                <td>{row.lat}</td>
                                <td>{row.long}</td>
                            </tr>
                        );

                    })}
                    </tbody>
                </table>
            );
        };

        return (
            <div style={center}>
                <form style={formStyle} onSubmit={this.handleSubmit.bind(this)}>
                    <div style={messageStyle} className={"message " + this.state.messageType}>
                        {this.state.message}
                    </div>
                    <input 
                        value={this.state.lat}
                        placeholder="lat" 
                        type="text" 
                        onChange={(e) => this.update('lat', e.target.value)}
                    />
                    <input
                        value={this.state.long}
                        placeholder="long"
                        type="text"
                        onChange={(e) => this.update('long', e.target.value)}
                    />
                    <input className="button" type="submit"/>
                </form>
                { !!( this.state.selfResult.length || this.state.postgresResult.length ) &&
                    <div style={result}>
                        <div className="u-full-width">
                            <p className="u-text-center">from <code>/get_using_self</code></p>
                            {renderData(this.state.selfResult)}
                        </div>
                        <div className="u-full-width">
                            <p className="u-text-center">from <code>/get_using_postgres</code></p>
                            {renderData(this.state.postgresResult)}
                        </div>
                    </div>
                }
            </div>
        );
    }
}

export default App;
