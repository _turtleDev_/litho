'use strict';

require('./components/dropdown');
require('./components/active-link');

import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/main.jsx';

const root = document.getElementById('app');

if ( root ) {
    ReactDOM.render(
        React.createElement(App),
        document.getElementById('app')
    );
}

