-- from 
-- http://stackoverflow.com/questions/33129526/loading-json-data-from-a-file-into-postgres

-- start a transaction
begin;

\set _filepath '/path/to/dump.json'
\set filepath '\'' :_filepath '\''

-- create geo relation
create table if not exists geo(name text, lat real, long real);

-- create a temporary table for holding our json data
create temporary table temp_json (values text) on commit drop;
copy temp_json from :filepath;

-- insert the data
insert into geo(lat, long, name) (
select CAST(values->>'lat' as real) as lat,
       CAST(values->>'long' as real) as long,
       values->>'name' as name
       from   (
               select json_array_elements(replace(values,'\','\\')::json) as values 
               from   temp_json
              ) a 
);

-- commit the transaction
commit;
