'use strict';

exports.register = function(plugin, options, next) {

    const Controllers = {
        oauth: require('../controllers/oauth.js')
    };

    plugin.route([

    {
        method: 'GET',
        path: '/v0/authorize',
        config: Controllers.oauth.authorize
    },
    {
        method: 'POST',
        path: '/v0/confirm',
        config: Controllers.oauth.grantAccess
    },
    {
        // we're only going to support POST requests for issuing tokens
        method: 'POST',
        path: '/v0/token',
        config: Controllers.oauth.token
    }

    ]);

    return next();
};

exports.register.attributes = {
    name: 'oauth routes',
    version: require('../../package.json').version
};
