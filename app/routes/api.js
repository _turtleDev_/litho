'use strict';

exports.register = function(plugin, options, next) {

    const Controllers = {
        api: require('../controllers/api')
    };

    plugin.route([

    {
        method: 'POST',
        path: '/post_location',
        config: Controllers.api.post_location
    },
    {
        method: 'GET',
        path: '/get_using_self',
        config: Controllers.api.get_using_self
    },
    {
        method: 'GET',
        path: '/get_using_postgres',
        config: Controllers.api.get_using_postgres
    },
    {
        method: 'GET',
        path: '/data',
        config: Controllers.api.data
    }
    ]);

    return next();
};

exports.register.attributes = {
    name: 'api routes',
    version: require('../../package.json').version
};
