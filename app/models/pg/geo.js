'use strict';

module.exports = function(client) {

    return new Promise((resolve) => {

        const tasks = [
            client.query('CREATE TABLE IF NOT EXISTS geo(lat real, long real, name text);'),
            client.query('CREATE EXTENSION IF NOT EXISTS cube'),
            client.query('CREATE EXTENSION IF NOT EXISTS earthdistance')
        ];
    
        Promise.all(tasks)
        .then(resolve)
        .catch((err) => { throw err; });
    });

};

