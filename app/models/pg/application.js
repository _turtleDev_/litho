'use strict';

/**
 * this is more or less stub code for registered applications
 */


/**
 * since we're going to test this on a local machine, maybe we can
 * get away with a local redirect?
 */

const client_port = 3000;

const dummyData = [
    '1',                                           // id
    'dummy_app',                                   // name
    '',                                            // website 
    `http://localhost:${client_port}/callback`,    // redirect uri
    '1',                                           // client id
    'thisistopsecret'                              // client secret
];

module.exports = function(client) {

    return new Promise((resolve) => {

        const init = '' +
        'CREATE TABLE IF NOT EXISTS application(' +
        'id int primary key,' +
        'app_name text,' +
        'website text,' +
        'redirect_uri text,' +
        'client_id text,' +
        'client_secret text);';
        
        const dummy = '' +
        'INSERT INTO application(id, app_name,website,redirect_uri,client_id,client_secret)' +
        'VALUES($1::int, $2::text, $3::text, $4::text, $5::text, $6::text)';

        const tasks = [
            client.query(init),
            client.query(dummy, dummyData)
        ];


        Promise.all(tasks)
        .then(resolve)
        .catch((err) => { 
            /**
             * from inspecting the error thrown on unique key violations.
             */
            if ( err.constraint !== 'application_pkey' ) {
                throw err; 
            } else {
                return resolve();
            }
        });
    });
};
