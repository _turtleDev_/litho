'use strict';

const Joi = require('joi');
const Boom = require('boom');
const Qs = require('qs');

/**
 * A generic store for handlers.
 *
 *
 * Handler.addHandler(type, fn) -> undefined
 *  type - handler for (aka for a request type)
 *  fn   - actual function
 *
 *  if type is null, the handler is stored as the default
 *  handler.
 *
 * Handler.getHandler(type) -> function
 *  type  - fetch handler of `type`
 *
 *  if type is null, then the default handler is returned
 *
 */
const Handlers = (function() {

    const _handlers = Object.create(null);

    // interface is a reserved keyword.
    const iface = {};

    let defaultHandler = function() {};

    iface.addHandler = function(type, fn) {

        if ( type === null ) {
            defaultHandler = fn;
        }

        if ( type in _handlers ) {
            throw new Error(`'${type}' handler already exists`);
        }

        _handlers[type] = fn;
    };

    iface.getHandler = function(type) {

        if ( !(type in _handlers) ) {
            console.error(`'${type}' handler not found. using default handler.`);
            return defaultHandler;
        }

        return _handlers[type];

    };

    return iface;
})();

Handlers.addHandler(null, function(request, reply) {

    return reply(Boom.notImplemented());
});

Handlers.addHandler('code', function(request, reply) {

    /**
     * authorization code flow
     *
     * We basically ask a user if it wants to grant a certain app access.
     * The result is not saved to database in our implementation, but a new token is issued.
     *
     * in layman speak, this means is that everytime you want the user to relogin, you'll
     * have to ask them to grant the app access again.
     */

    const constraint = Joi.object({
        response_type: Joi.string().required(),
        client_id: Joi.string().required(),
        redirect_uri: Joi.string(), // optional
        scope: Joi.string(),
        state: Joi.string()
    });

    const {error, value: options} = constraint.validate(request.query);

    if ( error ) {
        return reply(Boom.badRequest(error));
    }

    const query = 'select * from application where client_id = $1 limit 1';

    request.db.query(query, [options.client_id], function(err, result) {

        if ( !result.rows.length || err ) {
            return reply(Boom.badRequest('unauthorized application'));
        }

        result = result.rows[0];
        result = Object.assign({}, result, options);
        return reply.view('confirm', result);
    });
});

Handlers.addHandler('token', function(request, reply) {

    const constraint = Joi.object({
        response_type: Joi.string().required(),
        client_id: Joi.string().required(),
        redirect_uri: Joi.string()
    });

    const { error, value: options } = constraint.validate(request.query);

    if ( error )  {
        return reply(Boom.badRequest(error));
    }

    const query = 'select * from application where client_id = $1 limit 1';

    request.db.query(query, [options.client_id])
    .then((result) => {

        if ( !result.rows.length ) {
            return reply(Boom.badRequest('unauthorized application'));
        }

        result = result.rows[0];
        result = Object.assign({}, result, options);

        return reply.view('confirm', result);


    })
    .catch((err) => {

        console.error(err);
        return reply(Boom.internal(err));
    });
    
});

Handlers.addHandler('authorization_code', function(request, reply) {

    /**
     * exchange the authorization code grant for an access token.
     *
     */

    const constraint = Joi.object({
        client_id: Joi.string().required(),
        client_secret: Joi.string().required(),
        code: Joi.string().required(),
        redirect_uri: Joi.string(),
        state: Joi.string()
    });

    const { err, value: options } = constraint.validate(request.payload);

    if ( err ) {
        return reply(Boom.badRequest(err));
    }


    const query = '' +
    'SELECT * FROM application where ' +
    'client_id = $1::text and client_secret = $2::text';
    
    const values = [
        options.client_id,
        options.client_secret
    ];

    request.db.query(query, values)
    .then((result) => {

        if ( !result.rows.length ) {

            return reply(Boom.badRequest());
        }

        if ( !request.authority.grant.verify(options.code) ) {

            return reply(Boom.badRequest());
        } 

        const token = request.authority.token.new(options);
        const refreshToken = request.authority.refreshToken.new(options);
        const payload = {
            access_token: token,
            refresh_token: refreshToken,
            token_type: 'bearer'
        };

        /**
         * RFC 6749 - Section 5.1
         *
         * returns the (required) access_token and token_type as a JSON response
         */
        return reply(payload);
    })
    .catch((err) => {

        console.error(err);
        return reply(Boom.wrap(err));
    });
});

Handlers.addHandler('refresh_token', function(request, reply) {

    /**
     * refresh token flow
     *
     * RFC 6749 section 10.4
     *
     * issuing a refresh token is OPTIONAL.
     *
     * So refresh token are a sensitive issue, and it's best to employ
     * multiple security measures against their abuse, since they can
     * be used to invalidate other access tokens, and in turn generate
     * new tokens.
     *
     */

    const constraint = Joi.object({
        client_id: Joi.string().required(),
        client_secret: Joi.string().required(),
        refresh_token: Joi.string().required(),
    });

    const { error, value: options } = constraint.validate(request.payload);

    // should a refresh token invalidate previous tokens?

    if ( !request.authority.refreshToken.verify(options.refresh_token) ) {

        return reply(Boom.badRequest());
    }

    const payload = {
        access_token: request.authority.token.new(options),
        refresh_token: options.refresh_token,
        token_type: 'bearer'
    };

    return reply(payload);
});

Handlers.addHandler('confirm_code', function(request, reply) {

    /**
     * generate grants for authorization code flow
     *
     * we redirect the user agent to the redirection url the app registered, or
     * the url that it provided in the request
     */

    const grantId = request.authority.grant.new(request.query);
    const payload = {
        code: grantId,
        state: request.query.state || undefined
    };

    const uri = `${request.payload.redirect_uri}?${Qs.stringify(payload)}`;

    return reply.redirect(uri);
});

Handlers.addHandler('confirm_token', function(request, reply) {
    
    /**
     * generate the token for implicit auth flow
     */

    const token = request.authority.token.new(request.query);
    const payload = {
        access_token: token,
        token_type: 'bearer'
    };

    /**
     * RFC 6749 (oauth2) - section 4.2.2 
     *
     * The result of an implict auth should be returned as a fragment appended
     * to the callback uri
     */
    return reply.redirect(`${request.payload.redirect_uri}#${Qs.stringify(payload)}`);
});


exports.authorize = {
    description: 'oauth auth',
    validate: {
        query: Joi.object({
            response_type: Joi.string().required()
        }).unknown()
    },
    handler: function(request, reply) {

        /**
         * can be of type:
         *   'code' - Authorization Code
         *   'token' - Implicit Auth
         */
        const type = request.query.response_type;
        const handler = Handlers.getHandler(type);

        return handler(request, reply);
    }
};

exports.grantAccess = {
    description: 'generate a grant/token and send it back to the client',
    validate: {
        query: Joi.object({
            response_type: Joi.string().required(),
            client_id: Joi.string().required(),
            redirect_uri: Joi.string(), // optional
            scope: Joi.string(),
            state: Joi.string()
        }),
        payload: Joi.object({
            redirect_uri: Joi.string().required(),
            response_type: Joi.string().required()
        })
    },
    handler: function(request, reply) {

        /**
         * confirm_code - Authorization Code
         * confirm_token - Impiclit Auth
         */
        const responseType = `confirm_${request.payload.response_type}`;
        const handler = Handlers.getHandler(responseType);

        return handler(request, reply);
    }
};

exports.token = {
    description: 'issue a new token',
    validate: {
        payload: Joi.object({
            client_id: Joi.string().required(),
            client_secret: Joi.string(),
            grant_type: Joi.string().required(),
            code: Joi.string(),
            refresh_token: Joi.string(),
            redirect_uri: Joi.string()
        })
    },
    auth: false,
    handler: function(request, reply) {

        /**
         * grant types handles:
         * 'authorization_code' - Authorization code,
         * 'refresh_token' - same
         */
        const type = request.payload.grant_type;
        const handler = Handlers.getHandler(type);
        
        return handler(request, reply);
    }
};
