'use strict';

const Joi = require('joi');
const Mongoose = require('mongoose');
const User = Mongoose.model('User');

/**
 * Shameless copy pasta
 * http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
 */

function distanceBetween(point1, point2) {

    var { lat: lat1, long: lon1 } = point1;
    var { lat: lat2, long: lon2 } = point2;

    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
      return deg * (Math.PI/180);
}


exports.post_location = {
    description: 'create a new row in the database',
    validate: {
        payload: Joi.object({
            lat: Joi.number(),
            long: Joi.number(),
            name: Joi.string()
        })
    },
    handler: function(request, reply) {

        const payload = request.payload;

        const query = '' +
        'insert into geo(lat, long, name)' +
        'values($1::real, $2::real, $3::text)';

        const values = [ payload.lat, payload.long, payload.name ];


        request.db.query(query, values, function(err, result) {

            if ( err ) { throw err; }
            return reply(request.payload);
        });


    }
};


exports.get_using_self = {
    description: 'find points within a 5km radius of a point',
    validate: {
        query: Joi.object({
            lat: Joi.number().required(),
            long: Joi.number().required()
        })
    },
    handler: function(request, reply) {

        request.db.query('select * from geo', function(err, result) {
            
            const origin = {
                lat: request.query.lat,
                long: request.query.long
            };

            const rows = result.rows.filter(function(row) {

                let lat, long;
                const point = { lat, long } = row;

                return distanceBetween(origin, point) < 5;
            });

            return reply(rows);

        });
    }
};

exports.get_using_postgres = {
    description: 'find points within 5km radius, using postgres',
    validate: {
        query: Joi.object({
            lat: Joi.number().required(),
            long: Joi.number().required()
        })
    },
    handler: function(request, reply) {

        const origin = {
            lat: request.query.lat,
            long: request.query.long
        };

        const query = ''+
        'select * from geo where ' +
        'earth_box(ll_to_earth($1::real, $2::real), 5000) @> ' + 
        'll_to_earth(lat, long);';

        const values = [ origin.lat, origin.long ];


        request.db.query(query, values, function(err, result) {

            if ( err ) { throw err; }
            return reply(result.rows);
        });

    }
};

exports.data = {
    description: 'protected resourced, requires access_token for access',
    auth: 'token',
    handler: function(request, reply) {

        request.db.query('select * from geo')
        .then((result) => {

            return reply(result.rows);
        });
    }
};
