'use strict';

/**
 * A little word on authorities:
 *
 * The tokens/grants/entities that authorities generate are kept in memory
 * using a hash table.
 *
 * so everytime the server is restarted, all the generated tokens/grants are lost.
 *
 * ideally you'll want to keep these in a database, or atleast a redis cache.
 */

const crypto = require('crypto');

/**
 * A helper used for generating tokens and grant ids
 */
const stringReactor = function() {

    const hash = crypto.createHash('sha256');

    let n = 10000000 * Math.random();
    
    return hash.update(n.toString()).digest('hex').slice(0, 16);
};
    

/**
 * A generic store for issuing and
 * verifying entities
 */
class Authority {

    constructor() {
        this.store = Object.create(null);
    }

    new(payload) {

        let id;
        do  {
            id = stringReactor();
        } while ( id in this.store );

        this.store[id] = this.process(id, payload);

        return id;
    }

    verify(id) {

        if ( id in this.store ) {
            return true;
        }

        return false;
    }

    process(id, payload) {

        payload.id = id;
        return payload;
    }
}

class TokenAuthority extends Authority {


    process(id, payload) {

        return {
            id,
            expiry: (60 * 30 * 1000) + Date.now(),
            data: payload
        };
    }

    verify(id) {

        // token doesn't exist
        if ( !this.store[id] ) {
            return false;
        }

        // token has expired
        if ( Date.now() > this.store[id].expiry ) {
            this.store[id] = null;
            return false;
        }

        return true;
    }
}

class GrantAuthority extends Authority {

    process(id, payload) {
        return {
            id,
            expiry: (60 * 5 * 1000) + Date.now(),
            data: payload
        };
    }

    verify(id) {
        
        if ( !this.store[id] || Date.now() > this.store[id].expiry ) {
            this.store[id] = null;
            return false;
        }

        this.store[id] = null;
        return true;
    }
}

class RefreshTokenAuthority extends Authority {

    process(id, payload) {
        return {
            id,
            data: payload
        };
    }
}

exports.register = function(plugin, options, next) {

    const authority = {
        token: new TokenAuthority(),
        grant: new GrantAuthority(),
        refreshToken: new RefreshTokenAuthority
    };

    plugin.decorate('request', 'authority', authority);

    return next();
};

exports.register.attributes = {
    name: 'token authorities',
    version: require('../../package.json').version
};
