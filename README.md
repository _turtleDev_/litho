# Litho

> from `lithosphere` i.e. the earth's crust.

# installation

```
$ npm install               # or yarn install
$ npm i -g gulp             # gulp global install
$ $EDITOR config/config.js  # edit postgres and mongo configuration to match your machine
$ gulp                      # start the development server
```

# seeding the database
`dump.json` contains some initial geo-values you can use to start off with.

You can either manually copy those values, or you can use `import_data.sql` script to have them imported for you.
However, you will have to edit this file and change the value of `_filepath` to point to `dump.json`. If you're
on linux/osx, you might run into permission issues, so I'll recommend copying `dump.json` to `/tmp` and setting
`_filepath` to `/tmp/dump.json` then running  

```
psql -f import_data.sql
```

# OAuth2 server

litho implements a partial oauth2 server. The current snapshot currently
supports:

* Grants:
    * Authorization Code Grants
    * Implicit auth
    * Refresh Token auth
* Grant invalidation
* Token expiry
* Token acquisition using HTTP POST containing `application/x-form-urlencoded` http-body.

Unsupport/incomplete features:

* Error responses
    * currently, error's are signified by either a HTTP 400 Bad Request, or a HTTP 500 Internal Server Error
* Token acquisition using HTTP Basic Auth
* Validation of redirect\_uri(s)
* Security measure(s) to mitigate MITM and other attacks as described by the RFC.
* Application sign up. ( The server creates a dummy application to run tests with)
